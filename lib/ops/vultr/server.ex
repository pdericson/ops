defmodule Ops.Vultr.Server do
  def create(dcid, vpsplanid, osid, optional \\ %{}) do
    form = [{"DCID", dcid}, {"VPSPLANID", vpsplanid}, {"OSID", osid}]

    form =
      case optional do
        %{label: label} ->
          form ++ [{"label", label}]

        _ ->
          form
      end

    form =
      case optional do
        %{hostname: hostname} ->
          form ++ [{"hostname", hostname}]

        _ ->
          form
      end

    {:ok, response} =
      HTTPoison.post(
        "https://api.vultr.com/v1/server/create",
        {:form, form},
        [{"API-Key", System.get_env("VULTR_API_KEY")}]
      )

    %{body: body, status_code: 200} = response

    %{"SUBID" => subid} = Jason.decode!(body)

    {:ok, response, subid}
  end

  def destroy(subid) do
    {:ok, response} =
      HTTPoison.post(
        "https://api.vultr.com/v1/server/destroy",
        {:form, [{"SUBID", subid}]},
        [{"API-Key", System.get_env("VULTR_API_KEY")}]
      )

    {:ok, response}
  end
end
