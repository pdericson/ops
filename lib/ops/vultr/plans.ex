defmodule Ops.Vultr.Plans do
  def list() do
    {stdout, 0} = System.cmd("curl", ["-sS", "https://api.vultr.com/v1/plans/list"])
    Jason.decode!(stdout)
  end
end
