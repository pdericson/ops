defmodule Ops.Vultr.Regions do
  def list() do
    {stdout, 0} = System.cmd("curl", ["-sS", "https://api.vultr.com/v1/regions/list"])
    Jason.decode!(stdout)
  end
end
