defmodule Ops.Vultr.OS do
  def list() do
    {stdout, 0} = System.cmd("curl", ["-sS", "https://api.vultr.com/v1/os/list"])
    Jason.decode!(stdout)
  end
end
